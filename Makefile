# Makefile for C++ queue.
# Course: enel487
# Programmer: K. Naqvi

CC = g++-4.9
CCFLAGS = -Wall -g -std=c++11

testq : testq.o queue.o
	$(CC) $(CCFLAGS) -o $@ $^
queue.o : queue.cpp queue.h
testq.o : testq.cpp queue.h

%.o : %.cpp
	$(CC) -c $(CCFLAGS) $< -o $@

check: testq
	./$<

clean:
	rm -f *.o
	rm -f *~
	rm -f testq
